package com.onlineSnake;
import java.awt.Color;

public class Block {
    public static final int blockSize = 10;
    public int x;
    public int y;
    public Color color;
    public Block(int cx, int cy, Color col) {
        x = cx;
        y = cy;
        color = col;
    }


    public void set(int cx, int cy) {
        x = cx;
        y = cy;
    }
}
