package com.onlineSnake;
import java.util.*;
import java.awt.Color;

public class Body {
    public int x;
    public int y;
    public List<Block> blocks;
    private Color blockColor;
        
    public Body(int cx, int cy, Color color) {
        x = cx;
        y = cy;
        blockColor = color;
        blocks = new ArrayList<Block>();
        blocks.add(new Block(x,y,blockColor));
    }

    public void add() {
        blocks.add(new Block(-1, -1, blockColor));
    }
        
    public Block collis(Body body) {
        for (Block block : body.blocks) {
            if (block!= blocks.get(0) && x == block.x && y == block.y) return block; 
        }
        return null;
    }
        
}
