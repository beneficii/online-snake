package com.onlineSnake;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class ClientMain {

    public static void main(String[] args) {
        Scene mainScene = new Scene(40,40,150);
        JFrame window = new JFrame();
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setBounds(0, 0, 500, 500);

        window.getContentPane().add(mainScene);
        window.setVisible(true);

        while(!mainScene.gameOver) {
            mainScene.cycle();
            mainScene.repaint();
            try {
                Thread.sleep(mainScene.cycleDelay);
            } catch (InterruptedException e) {}
        }
        JOptionPane.showMessageDialog(window, "You lost!");

    }
}
