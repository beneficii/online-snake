package com.onlineSnake;
import java.util.*;
import java.awt.Color;

public class Food extends Body {
    public Food() {
        super(0, 0, Color.red); 
        Random rand = new Random();
        x = rand.nextInt(40);
        y = rand.nextInt(40);
        blocks.get(0).set(x,y);
    }

    public void newPos() {
        Random rand = new Random();
        x = rand.nextInt(40);
        y = rand.nextInt(40);
        blocks.get(0).set(x,y);
    }
}
