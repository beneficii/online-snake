package com.onlineSnake;
import java.util.*;
import java.awt.Graphics;
import java.awt.Color;
import javax.swing.JComponent;
import javax.swing.JFrame;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

@SuppressWarnings("serial")
public class Scene extends JComponent {
        
    public List<List<Block>> blocks;
    public int width;
    public int height;
    public int cycleDelay;
    public Snake snake;
    public Food food;
    public boolean gameOver;
        
    public Scene(int w, int h, int delay) {
        width = w; //40 so far
        height = h; //40 so far
        cycleDelay = delay; //in ms
        gameOver = false;
        blocks = new ArrayList<List<Block>>();

        snake = new Snake(10,10, Color.yellow);
        blocks.add(snake.blocks);
        food = new Food();
        blocks.add(food.blocks);

        KeyListener listener = new MyKeyListener();
        addKeyListener(listener);
        setFocusable(true);
    }

    public void paint(Graphics g) {
        g.setColor(Color.black);
        g.fillRect (0, 0, 400, 400);  
        for (List<Block> bSet : blocks) {
            for (Block b : bSet) {
                g.setColor(b.color);
                int bs = b.blockSize;
                g.fillRect(b.x*bs, b.y*bs, bs, bs);
            }
        }
    }
        
    public void cycle() {
        snake.move();
        if (snake.collis(food) != null) {
            food.newPos();
            snake.grow();
        }
        if (snake.collis(snake)!= null)
            gameOver = true;
    }

    public class MyKeyListener implements KeyListener {
        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
            switch (e.getKeyCode()) {
                case KeyEvent.VK_A:
                    snake.turnLeft();
                    break;
                case KeyEvent.VK_D:
                    snake.turnRight();
                    break;
                default:break;
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
            
        }
    }
}
