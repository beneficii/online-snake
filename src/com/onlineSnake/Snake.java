package com.onlineSnake;

import java.awt.Color;

public class Snake extends Body {
    Direction direction;

    public Snake(int x, int y, Color color) {
        super(x, y, color);
        add();
        add();
        direction = Direction.EAST;
    }   

    public void move() {
        Block last = blocks.remove(blocks.size()-1);
        last.set(x,y);
        blocks.add(1, last);
        switch (direction) {
            case EAST: x += 1; if (x>39) x = 0; break;
            case WEST: x -= 1; if (x<0) x = 39; break;
            case SOUTH: y += 1; if (y>39) y = 0; break;
            case NORTH: y -= 1; if (y<0) y = 39; break;
            default:break;
        }
        blocks.get(0).set(x,y);
    }

    public void turnLeft() {
        direction = direction.left();
    }

    public void turnRight() {
        direction = direction.right();
    }

    public void grow() {
        add();
    }

    enum Direction {
        NORTH, EAST, SOUTH, WEST;

        public Direction left() {
            return values()[(ordinal() + 3) % 4];
        }

        public Direction right() {
            return values()[(ordinal() + 1) % 4];
        }
    }
}
